import {Component} from "react";
import {withRouter} from "react-router-dom";

class OrderConfirmed extends Component{
    constructor(props) {
        super(props);
    }
    render (){
        return <h2 className="OrderPlaced">Order Placed Successfully</h2>
    }
}
export default withRouter(OrderConfirmed);