import {Component} from "react";
import {Redirect, withRouter} from "react-router-dom";
import './OrderSummary.css'
import {Button} from "react-bootstrap";

class OrderSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {summary: {}, isOrderConfirmed: false, orderPlacedMessage: {}}
    }

    componentDidMount() {
        const summary = this.props.location.state.summary
        this.setState({summary: summary});
    }

    async putData(url) {
        const response = await fetch(url, {
            method: 'PUT'
        });
        return response.json()
            .then(json => this.setState({orderPlacedMessage: json}));
    }

    async handleClick() {
        const url = `https://bookworm-shack-staging.herokuapp.com/books/order/${this.state.summary.orderId}/confirm`
        await this.putData(url);
        console.log(this.state.orderPlacedMessage.message);
        if (this.state.orderPlacedMessage.message === "Order Confirmed")
            this.setState({isOrderConfirmed: true})

    }

    checkInvalidOrderMessage() {
        if (!this.state.isOrderValid)
            return (this.state.orderPlacedMessage.message);
    }

    render() {
        if (this.state.isOrderConfirmed) {
            return (
                <Redirect to={{
                    pathname: '/order/Confirmed'
                }}
                />
            )
        }
        return (
            <div className='OrderSummary'>
                <h1>Review Your Order</h1>
                <div className='centerBox'>
                <div className="ShippingDetails">
                    <h2>Shipping Details</h2>
                    <p>Name : {this.state.summary.name}</p>
                    <p>Contact : {this.state.summary.mobileNumber}</p>
                    <p>Address : {this.state.summary.address}</p>
                    <p>Country : {this.state.summary.country}</p>
                </div>
                <div className="OrderDetails">
                    <h2>Order Details</h2>
                    <p>Order ID : {this.state.summary.orderId}</p>
                    <p>Book Name : {this.state.summary.bookName}</p>
                    <p>Quantity : {this.state.summary.bookCount}</p>
                    <p>Total Amount : ₹{this.state.summary.totalAmount}</p>
                </div>

                <br/>
                </div>
                <Button className='PlaceYourOrder' onClick={this.handleClick.bind(this)}>Place Your Order</Button>
                <p>{this.checkInvalidOrderMessage()}</p>
            </div>

        )
    }
}

export default withRouter(OrderSummary)