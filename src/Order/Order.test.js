import {fireEvent, render, screen} from '@testing-library/react';
import Order from "./Order";
import {Route, Router} from "react-router-dom";
import {createMemoryHistory} from "history";

test("does have a form ", () => {
    const history = createMemoryHistory()
    history.push('/order/3/5')
    render(
        <Router history={history}>
            <Route exact path="/order/:bookId/:bookCount">
                <Order/>
            </Route>
        </Router>
    )
    expect(screen.getByRole('form')).toBeInTheDocument();
})

test("has inputs to receive new delivery details", () => {
    const history = createMemoryHistory()
    history.push('/order/3/5')
    render(
        <Router history={history}>
            <Route exact path="/order/:bookId/:bookCount">
                <Order/>
            </Route>
        </Router>
    )

    expect(screen.getByPlaceholderText("Enter Your Name")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Enter Your Email")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Enter Your Address")).toBeInTheDocument();
    expect(screen.getByLabelText("Email:")).toBeInTheDocument();
    expect(screen.getByRole('button', {name: "Add Details"})).toBeInTheDocument();

})

test("should submit the values filled", () => {
    const history = createMemoryHistory()
    history.push('/order/3/5')
    render(
        <Router history={history}>
            <Route exact path="/order/:bookId/:bookCount">
                <Order/>
            </Route>
        </Router>
    )
    let name = screen.getByLabelText("Name:");
    let submitButton = screen.getByRole("button", {name: "Add Details"});

    fireEvent.change(name,{target:{value:"test Name"}})
   fireEvent.click(submitButton);
    expect(name.value).toBe("test Name");

})
