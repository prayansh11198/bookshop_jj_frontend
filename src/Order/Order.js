import {Component} from "react";
import {Redirect, withRouter} from "react-router-dom";
import "./Order.css"

class Order extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookId: null, bookCount: null, name: null,
            email: null, mobileNumber: null, address: null, country: null,
            summary: {},
            isSubmitted: false,
            isOrderValid:true
        }
    }

    componentDidMount() {
        const bookId = this.props.match.params.bookId;
        const bookCount = this.props.match.params.bookCount;
        this.setState({bookId: bookId, bookCount: bookCount})
    }

    handleInputChange(evt) {
        this.setState({[evt.target.name]: evt.target.value})
    }

    async postData(url, data) {

        const response = await fetch(url, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data)
        });
        return response.json()
            .then(json => this.setState({summary: json}))
    }

    async handleFormSubmit(evt) {
        evt.preventDefault()
        const url = 'https://bookworm-shack-staging.herokuapp.com/books/order';
        const {bookId, address, country, name, email, bookCount, mobileNumber} = this.state;
        const data = {name, email, mobileNumber, address, country, bookId, bookCount};
        await this.postData(url, data)
        console.log(this.state.summary);
        if(this.state.summary.message!=null) {
            this.setState({isOrderValid: false})
        }
        else{
            this.setState({isOrderValid: true})
        this.setState({isSubmitted: true})}
    }
    checkInvalidOrderMessage() {
        if (!this.state.isOrderValid)
            return (this.state.summary.message);

    }

    render() {
        if (this.state.isSubmitted && this.state.isOrderValid) {
            return (
                <Redirect to={{
                    pathname: '/order/summary',
                    state: {summary: this.state.summary}
                }}
                />
            )
        }

        return (
            <div>
                <form className="form" name="order-form" onSubmit={this.handleFormSubmit.bind(this)} method="post"
                      action='https://bookworm-shack-staging.herokuapp.com/books/order'>
                    <label>
                        Name:
                        <input
                            value={this.state.name}
                            name="name"
                            type="text"
                            placeholder="Enter Your Name"
                            onChange={this.handleInputChange.bind(this)}
                            required
                        />
                    </label>
                    <br/>

                    <label>
                        Email:
                        <input
                            value={this.state.email}
                            name="email"
                            type="text"
                            placeholder="Enter Your Email"
                            onChange={this.handleInputChange.bind(this)}
                            required
                        />
                    </label>
                    <br/>
                    <label>
                        Mobile:
                        <input
                            value={this.state.mobileNumber}
                            name="mobileNumber"
                            type="text"
                            placeholder="Enter Your Mobile number"
                            onChange={this.handleInputChange.bind(this)}
                            required
                        />
                    </label>
                    <br/>
                    <label>
                        Address:
                        <input
                            value={this.state.address}
                            name="address"
                            type="text"
                            placeholder="Enter Your Address"
                            onChange={this.handleInputChange.bind(this)}
                            required
                        />
                    </label>
                    <br/>
                    <label>
                        Country:
                        <input
                            value={this.state.country}
                            name="country"
                            type="text"
                            placeholder="Enter Your Country"
                            onChange={this.handleInputChange.bind(this)}
                            required
                        />
                    </label>
                    <br/>
                    <label>
                        BookCount:
                        <input
                            value={this.state.bookCount}
                            name="bookCount"
                            type="number"
                            min="0"
                            placeholder="Enter Book Count"
                            onChange={this.handleInputChange.bind(this)}
                            required
                        />
                    </label>
                    <br/>
                    <input type="submit" value="Add Details"/>
                </form>
                <p >{this.checkInvalidOrderMessage()}</p>
            </div>
        )
    };

}

export default withRouter(Order);