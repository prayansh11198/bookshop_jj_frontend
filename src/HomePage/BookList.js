import {useState} from "react";
import {Link} from "react-router-dom";
import PaginatedBookList from "./PaginatedBookList";
import './BookList.css'


function BookList() {
    const [pageNumber, setPageNumber] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const MAX_SIZE = 5000;
    const pageHandler = (offset) => {
        if (pageNumber + offset <= totalPages) {
            setPageNumber(pageNumber + offset);
        }
    }
    const previousPageHandler = () => {
        if (pageNumber > 1) {
            setPageNumber(pageNumber - 1);
        }
    }
    const getLength = () => {
        fetch(`https://bookworm-shack-staging.herokuapp.com/books?pageSize=${MAX_SIZE}&pageNumber=1`)
            .then(response => response.json())
            .then((books) => {
                setTotalPages(Math.ceil(books.length/9))
            });
    }
    return (<div>
            {getLength()}
            <PaginatedBookList pageNumber={pageNumber}/>
            <nav className={"container nav-container"}>
                <ul className="pagination">
                    <li className="page-item" onClick={previousPageHandler}>
                        <Link to={`/?pageNumber=${pageNumber}`}>Previous</Link>
                    </li>
                    <li className="page-item active">
                        <Link to={`/?pageNumber=${pageNumber}`}>{pageNumber}</Link>
                    </li>
                    <li onClick={() => pageHandler(1)}>
                        {pageNumber + 1 <= totalPages &&
                        <Link to={`/?pageNumber=${pageNumber + 1}`}>{pageNumber + 1}</Link>
                        }
                    </li>
                    <li className="page-item" onClick={() => pageHandler(2)}>
                        {pageNumber + 2 <= totalPages &&
                        <Link to={`/?pageNumber=${pageNumber + 2}`}>{pageNumber + 2}</Link>
                        }
                    </li>
                    <li onClick={() => pageHandler(1)}>
                        <Link to={`/?pageNumber=${pageNumber + 1}`}>Next</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default BookList;