import {render, screen, waitFor} from "@testing-library/react";
import PaginatedBookList from "./PaginatedBookList";

beforeAll(() => jest.spyOn(window, 'fetch'))

test('Should render loading while loading',async () => {
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => [],
    });
    render(<PaginatedBookList pageNumber={1}/>);

    expect(screen.getByText('Loading...')).toBeInTheDocument();
})

test('renders list of books', async () => {
    const response = [{name:'name', authorName:'authorName', price: 190, imageUrl:''}];
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });

    render(<PaginatedBookList pageNumber={1}/>);

    expect(window.fetch).toHaveBeenCalledWith('https://bookworm-shack-staging.herokuapp.com/books?pageSize=9&pageNumber=1');
    expect(window.fetch).toHaveBeenCalledTimes(1);

    await waitFor(() => screen.getByText('name'));

    expect(screen.getByText('By authorName')).toBeInTheDocument();
    expect(screen.getByText('₹190')).toBeInTheDocument();
});