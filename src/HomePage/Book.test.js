import {render, screen} from "@testing-library/react";
import Book from "./Book";

test('Renders Book', () => {
    const book = {
        "name": "City of Bones (The Mortal Instruments, #1)1",
        "price": 14611,
        "authorName": "Cassandra Clare1",
        "imageUrl": "https://images.gr-assets.com/books/1432730315m/256683.jpg"
    };
    render(<Book book={book}/>);
    expect(screen.getByText('City of Bones (The Mortal Instruments, #1)1')).toBeInTheDocument();
    expect(screen.getByText('₹14611')).toBeInTheDocument();
    expect(screen.getByText('By Cassandra Clare1')).toBeInTheDocument();
});

