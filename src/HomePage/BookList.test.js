import BookList from "./BookList";
import {render, screen} from "@testing-library/react";
import {Router} from "react-router";
import {createMemoryHistory} from "history";

test('renders pagination bar', () =>{
    const history = createMemoryHistory()
    render(
        <Router history={history}>
            <BookList />
        </Router>,
    )
    expect(screen.getAllByRole('listitem').length).toBe(5);
})

test('test link tags', () =>{
    const history = createMemoryHistory()
    render(
        <Router history={history}>
            <BookList />
        </Router>,
    )
    expect(screen.queryAllByRole('link').length).toBe(3);
    expect(screen.getByRole('link', {name: 1}));
    expect(screen.getByRole('link', {name: 'Next'}));
    expect(screen.getByRole('link', {name: 'Previous'}));
})