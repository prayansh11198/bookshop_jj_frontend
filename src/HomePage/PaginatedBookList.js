import {Component} from "react";
import Book from "./Book";

export default class PaginatedBookList extends Component {
    constructor(props) {
        super(props);
        this.state = {books: [], loading:false}
    }

    static get URL() {
        return 'https://bookworm-shack-staging.herokuapp.com/books?pageSize=9';
    }

    updateBooks = (data) => {
        this.setState({books: data,loading:true});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.pageNumber !== prevProps.pageNumber){
            const promise = fetch(`${PaginatedBookList.URL}&pageNumber=${this.props.pageNumber}`);
            promise
                .then(response => response.json())
                .then(this.updateBooks)
        }
    }

    componentDidMount() {
        let pageNumber = this.props.pageNumber;
        if(pageNumber ===null ){
            pageNumber = 1;
        }
        const promise = fetch(`${PaginatedBookList.URL}&pageNumber=${pageNumber}`);
        promise
            .then(response => response.json())
            .then(this.updateBooks)
    }

    render() {
        if(!this.state.loading){
            return <div>Loading...</div>
        }
        return (
            <div className={"container"}>
                {
                    this.state.books.map((book)=>
                        <Book book={book}/>
                    )
                }
            </div>
        )
    }
}