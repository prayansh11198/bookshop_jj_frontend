import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import BaseTemplate from "./BaseTemplate/BaseTemplate";
import './BaseTemplate/BaseTemplate.css';

ReactDOM.render(
    <React.StrictMode>
        <div className="container-fluid tempmargin">
        <BaseTemplate/>
        </div>
        <App/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
