import {render, screen} from '@testing-library/react';
import CustomNavbar from "./CustomNavbar";


test('should show Home and Contact ', () => {
    render(<CustomNavbar />);
    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('Contact')).toBeInTheDocument();
});