import {Component} from "react";
import '../BaseTemplate.css';

class CustomNavbar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            // <Navbar bg="dark" variant="dark">
            //     <Container>
            //         <Navbar.Brand href="#home">Navbar</Navbar.Brand>
            //         <Nav className="me-auto">
            //             <Nav.Link href="#home">Home</Nav.Link>
            //             <Nav.Link href="#features">Features</Nav.Link>
            //             <Nav.Link href="#pricing">Pricing</Nav.Link>
            //         </Nav>
            //     </Container>
            // </Navbar>
            <div className="row navmag">
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav">
                            <li><a href="/">Home</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default CustomNavbar;