import {Component} from "react";
import './BaseTemplate.css';
import SearchAndAccountBar from "./SearchAndAccountBar/SearchAndAccountBar";
import CustomNavbar from "./CustomNavbar/CustomNavbar";

class BaseTemplate extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row back">
                    <div className="col-md-10 col-md-push-1 whole">
                        <SearchAndAccountBar/>
                        <CustomNavbar />
                    </div>
                </div>
            </div>
    );
    }
}

export default BaseTemplate;