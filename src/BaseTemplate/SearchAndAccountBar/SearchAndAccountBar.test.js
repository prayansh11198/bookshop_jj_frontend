import {render, screen} from '@testing-library/react';
import SearchAndAccountBar from "./SearchAndAccountBar";


test('should show account', () => {
    render(<SearchAndAccountBar />);
    expect(screen.getByText('Account')).toBeInTheDocument();
});

test('should show search bar', () => {
    render(<SearchAndAccountBar />);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
});