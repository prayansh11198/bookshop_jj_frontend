import {Component} from "react";
import '../BaseTemplate.css';
import SearchBar from "./SearchBar/SearchBar";
import CreateAccount from "./CreateAccount/CreateAccount";


class SearchAndAccountBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row marg">
                <div className="col-md-3 col-md-push-1">
                    <div className="shrvenk"></div>
                </div>
                <SearchBar/>
                <CreateAccount/>
            </div>
        );
    }
}
export default SearchAndAccountBar;