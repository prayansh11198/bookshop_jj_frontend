import {Component} from "react";
import '../../BaseTemplate.css';
import {Button, Col, Container, Row} from "react-bootstrap";


class CreateAccountModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div className="modal fade" id="create_account">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close"
                                data-dismiss="modal">&times;</button>
                        <h3 className="modal-title">Create new Account</h3>
                    </div>
                    <form role="form" action="#" method="POST" className="post-form">
                        <div className="modal-body">
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Username"
                                       name="username" id="username"/>
                            </div>
                            <div className="form-group">
                                <input type="email" className="form-control" placeholder="Email Id"
                                       name="email" id="email"/>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control"
                                       placeholder="password" name="password" id="password"/>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-primary btn-block">Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>);
    }
}
export default CreateAccountModal;