import {Component} from "react";
import '../../BaseTemplate.css';


class SignInModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="modal fade" id="sign_in">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close"
                                    data-dismiss="modal">&times;</button>
                            <h3 className="modal-title">Log In</h3>
                        </div>
                        <form role="form" action="#" method="POST" className="post-form">
                            <div className="modal-body">
                                <div className="form-group">
                                    <input type="username" className="form-control"
                                           placeholder="username" name="username"/>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control"
                                           placeholder="password" name="password"/>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-primary btn-block">Log In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default SignInModal;