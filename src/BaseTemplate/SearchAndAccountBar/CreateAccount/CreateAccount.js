import {Component} from "react";
import '../../BaseTemplate.css';
import DropdownToggle from "react-bootstrap/DropdownToggle";
import DropdownMenu from "react-bootstrap/DropdownMenu";
import DropdownItem from "react-bootstrap/DropdownItem";
import {Dropdown} from "react-bootstrap";


class CreateAccount extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
                <div className="col-md-2 col-md-push-2 givemargin">
                    <ul className="nav navbar-nav navbar-right">
                        <Dropdown className="dropdown">
                            <DropdownToggle href="#" className="dropdown-toggle accnt" data-toggle="dropdown"
                               className="nav-a nav-a-2 nav-truncate">
                                <span className="nav-line-2 spn">Account</span>
                            </DropdownToggle>
                            <DropdownMenu className="dropdown-menu acc">
                                <DropdownItem className="sign_in">
                                    <button type="button" className="btn btn-success" data-toggle="modal"
                                            data-target="#sign_in">
                                        Sign In
                                    </button>
                                </DropdownItem>
                                <DropdownItem className="create_account">
                                    <h5>New to Shrvenk ?<a href="" data-toggle="modal" zdata-target="#create_account">Create
                                        Account</a></h5>
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </ul>
                </div>
        );
    }
}

export default CreateAccount;
