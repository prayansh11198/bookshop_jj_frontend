import {fireEvent, render, screen} from '@testing-library/react';
import CreateAccount from "./CreateAccount";



test('should show Create Account', () =>{
    render(<CreateAccount />);
    expect(screen.getByText('Account')).toBeInTheDocument();
});

test('should show sign in', () => {
    render(<CreateAccount />);
    fireEvent.click(screen.getByText('Account'));
    expect(screen.getByText(/Sign in/i)).toBeInTheDocument();
});