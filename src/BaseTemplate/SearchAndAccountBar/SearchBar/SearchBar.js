import {Component} from "react";
import '../../BaseTemplate.css';

class SearchBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-md-4 col-md-push-1">
                <form role="form" method="POST" action="#">
                    <div className="active-pink-3 active-pink-4 mb-4">
                        <input className="form-control srh" name='str' type="text"
                               placeholder="Search by Name or Ingredients" aria-label="Search"/>
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchBar;