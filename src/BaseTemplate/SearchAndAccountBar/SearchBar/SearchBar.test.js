import {render, screen} from '@testing-library/react';
import SearchBar from "./SearchBar";


test('should show form', () => {
    render(<SearchBar />);
    expect(screen.getByRole('form')).toBeInTheDocument();
});

test('should show search bar', () => {
    render(<SearchBar />);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
});