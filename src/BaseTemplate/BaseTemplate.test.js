import {render, screen} from '@testing-library/react';
import BaseTemplate from "./BaseTemplate";


test('should show Home and Contact', () => {
    render(<BaseTemplate />);
    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('Contact')).toBeInTheDocument();
});