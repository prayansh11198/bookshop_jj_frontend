import './App.css';
import BookDetails from "./BookDetails/BookDetails";
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import BookList from "./HomePage/BookList";
import Order from "./Order/Order";
import OrderSummary from "./Order/OrderSummary";
import OrderConfirmed from "./Order/OrderConfirmed";

function App() {
  return (
      <Router>
          <div>
              <Switch>
                  <Route exact path="/order/:bookId/:bookCount">
                      <Order/>
                  </Route>
                  <Route exact path="/order/summary">
                      <OrderSummary />
                  </Route>
                  <Route exact path="/order/confirmed">
                      <OrderConfirmed />
                  </Route>
                  <Route path="/books/:bookId">
                      <BookDetails />
                  </Route>
                  <Route path="/">
                      <BookList/>
                  </Route>
              </Switch>
          </div>
      </Router>
  );
}

export default App;
