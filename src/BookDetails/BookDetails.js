import {Component} from "react";
import './BookDetails.css';
import {Col, Container, Row} from "react-bootstrap";
import Fade from "react-reveal/Fade";
import {Link, withRouter} from "react-router-dom";
import runtimeEnv from '@mars/heroku-js-runtime-env';


class BookDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookDetails: null,
            loading: true,
            emptyDetails: false,
            buyQuantity: null,
        }
    }

    setDetails = (data) => {
        if (data !== null) {
            console.log(data);
            return this.setState(
                {
                    bookDetails: data,
                    loading: false
                }
            )
        } else {
            return this.setState(
                {
                    emptyDetails: true,
                }
            )
        }
    }

    async componentDidMount() {
        const env = runtimeEnv();
        let URL = env.REACT_APP_API_URL + 'books/';
        let bookId = this.props.match.params.bookId;
        const response = await fetch(URL + bookId);
        const parsedJson = await response.json();
        this.setDetails(parsedJson);
    }

    setQuantity = (e) => {
        this.setState(
            {
                buyQuantity: e.target.value,
            }
        );
    }

    render() {

        if (this.state.emptyDetails) {
            return <div className="text-center" style={{fontSize: "2rem", color: "white"}}>Book not found</div>
        }
        if (this.state.loading) {
            return <div className="text-center" style={{fontSize: "2rem", color: "white"}}>Loading.....</div>
        }
        const {isbnA, imageUrl, isbn, price, ratings, name, authorName, reviews, booksCount} = this.state.bookDetails;
        return (
            <Container className="parent-div">
                <div id="details">
                    <Row className="my-5">
                        <Col md={4} className="text-center">
                            <img className="image" src={imageUrl} alt="book"/>
                        </Col>
                        <Col md={8}>
                            <Fade bottom>
                                <div>
                                    <h1 className="name">{name}</h1>
                                    <div className="author-name"><span>by {authorName}</span></div>
                                    <hr className="line"/>
                                    <div className="details">
                                        <Row style={{marginLeft: "25px"}}>
                                            <Row>
                                                <Col style={{
                                                    textAlign: "right",
                                                    marginTop: "10px",
                                                    paddingRight: "0px"
                                                }}
                                                     md={2}>Price:</Col>
                                                <Col md={4} className="priceVal">&#x20b9;{price}</Col>
                                            </Row>
                                            <Col md={2} className="left-details-col">
                                                <Row className="row-pad">Ratings:</Row>
                                                <Row className="row-pad">Review:</Row>
                                                <Row className="row-pad">Availability:</Row>
                                                <Row className="row-pad">ISBN:</Row>
                                                <Row className="row-pad">ISBN-A:</Row>
                                            </Col>
                                            <Col md={4} className="right-details-col">
                                                <Row className="row-pad">{ratings}</Row>
                                                <Row className="row-pad">{reviews}</Row>
                                                <Row className="row-pad">{booksCount}</Row>
                                                <Row className="row-pad">{isbn}</Row>
                                                <Row className="row-pad">{isbnA}</Row>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </Fade>
                        </Col>
                    </Row>
                    <Row className="my-5 btn-row">
                        <Col md={5} className="my-auto col-left">
                            <div className="quantity">
                                <span>Quantity: </span>
                                <input type="number" min="0"
                                       onChange={this.setQuantity}
                                       placeholder="Enter Qty"
                                       style={{width: "65px", textAlign: "center"}}
                                       defaultValue="1"
                                />
                            </div>
                            <Link to={`/order/${this.props.match.params.bookId}/${this.state.buyQuantity}`}
                                  className="btn buyBtn rounded-pill d-flex justify-content-center align-items-center"
                                  style={{backgroundColor: 'deepskyblue'}}
                            >
                                <span className="buyText">Buy</span>
                            </Link>
                        </Col>
                        <Col md={8} className="text-center d-flex justify-content-left col-right">

                        </Col>
                    </Row>
                </div>
            </Container>
        );
    }
}

export default withRouter(BookDetails);