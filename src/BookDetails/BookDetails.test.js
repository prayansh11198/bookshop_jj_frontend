import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import BookDetails from "./BookDetails";
import {Route, Router} from "react-router-dom";
import {createMemoryHistory} from "history";
import runtimeEnv from "@mars/heroku-js-runtime-env";

beforeAll(() => jest.spyOn(window, 'fetch'))

test('renders loading when the fetch list is empty', () => {
    const history = createMemoryHistory()
    history.push('/books/3')

    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => [],
    });

    render(
        <Router history={history}>
            <Route path="/books/:bookId">
                <BookDetails/>
            </Route>
        </Router>
    );

    expect(screen.getByText('Loading.....')).toBeInTheDocument();
});

test('test for fetch call', async () => {
    const history = createMemoryHistory()
    history.push('/books/3')
    const response = {
        id: 3,
        name: "City of Bones (The Mortal Instruments, #1)",
        authorName: "Cassandra Clare",
        price: 400,
        imageUrl: "https://images.gr-assets.com/books/1432730315m/256683.jpg",
        booksCount: 178,
        isbn: "1416914285",
        isbnA: "9781416914280",
        ratings: 4.12,
        review: "review",
    };
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });

    render(
        <Router history={history}>
            <Route path="/books/:bookId">
                <BookDetails/>
            </Route>
        </Router>
    );

    const env = runtimeEnv();
    let URL=env.REACT_APP_API_URL+'books/3';

    expect(window.fetch).toHaveBeenCalledWith(URL);
    expect(window.fetch).toHaveBeenCalledTimes(1);
});


test('test for No book found', async () => {
    const history = createMemoryHistory()
    history.push('/books/1')
    const response = null;
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });

    render(
        <Router history={history}>
            <Route path="/books/:bookId">
                <BookDetails/>
            </Route>
        </Router>
    );

    const env = runtimeEnv();
    let URL=env.REACT_APP_API_URL+'books/1';

    expect(window.fetch).toHaveBeenCalledWith(URL);
    expect(window.fetch).toHaveBeenCalledTimes(1);

    await waitFor(() => screen.getByText('Book not found'));
});

test('test for book title details', async () => {
    const history = createMemoryHistory()
    history.push('/books/3')
    const response = {
        id: 3,
        name: "City of Bones (The Mortal Instruments, #1)",
        authorName: "Cassandra Clare",
        price: 400,
        imageUrl: "https://images.gr-assets.com/books/1432730315m/256683.jpg",
        booksCount: 178,
        isbn: "1416914285",
        isbnA: "9781416914280",
        ratings: 4.12,
        review: "review",
    };
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });

    render(
        <Router history={history}>
            <Route path="/books/:bookId">
                <BookDetails/>
            </Route>
        </Router>
    );

    const env = runtimeEnv();
    let URL=env.REACT_APP_API_URL+'books/3';

    expect(window.fetch).toHaveBeenCalledWith(URL);
    expect(window.fetch).toHaveBeenCalledTimes(1);

    await waitFor(() => screen.getByText('City of Bones (The Mortal Instruments, #1)'));
    expect(screen.getByText('City of Bones (The Mortal Instruments, #1)')).toBeInTheDocument();
});

test('test for price details', async () => {
    const history = createMemoryHistory()
    history.push('/books/3')
    const response = {
        id: 3,
        name: "City of Bones (The Mortal Instruments, #1)",
        authorName: "Cassandra Clare",
        price: 400,
        imageUrl: "https://images.gr-assets.com/books/1432730315m/256683.jpg",
        booksCount: 178,
        isbn: "1416914285",
        isbnA: "9781416914280",
        ratings: 4.12,
        review: "review",
    };
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });

    render(
        <Router history={history}>
            <Route path="/books/:bookId">
                <BookDetails/>
            </Route>
        </Router>
    );

    const env = runtimeEnv();
    let URL=env.REACT_APP_API_URL+'books/3';

    expect(window.fetch).toHaveBeenCalledWith(URL);
    expect(window.fetch).toHaveBeenCalledTimes(1);

    await waitFor(() => screen.getByText('City of Bones (The Mortal Instruments, #1)'));
    expect(screen.getByText('₹400')).toBeInTheDocument();
});

test('test for Buy Button', async () => {
    const history = createMemoryHistory()
    history.push('/books/3')
    const response = {
        id: 3,
        name: "City of Bones (The Mortal Instruments, #1)",
        authorName: "Cassandra Clare",
        price: 400,
        imageUrl: "https://images.gr-assets.com/books/1432730315m/256683.jpg",
        booksCount: 178,
        isbn: "1416914285",
        isbnA: "9781416914280",
        ratings: 4.12,
        review: "review",
    };
    window.fetch.mockResolvedValueOnce({
        ok: true,
        json: async () => response,
    });

    const { queryByPlaceholderText } = render(
        <Router history={history}>
            <Route path="/books/:bookId">
                <BookDetails/>
            </Route>
        </Router>
    );

    const env = runtimeEnv();
    let URL=env.REACT_APP_API_URL+'books/3';

    expect(window.fetch).toHaveBeenCalledWith(URL);
    expect(window.fetch).toHaveBeenCalledTimes(1);

    await waitFor(() => screen.getByText('City of Bones (The Mortal Instruments, #1)'));

    const buyBtn = screen.getByText('Buy');
    const quantityInput = queryByPlaceholderText('Enter Qty');

    fireEvent.change(quantityInput, {target : {value : '5'}});
    fireEvent.click(buyBtn);

    expect(history.location.pathname).toEqual('/order/3/5');
});

